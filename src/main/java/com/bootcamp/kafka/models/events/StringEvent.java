package com.bootcamp.kafka.models.events;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class StringEvent extends Event<String>  {
}