package com.bootcamp.transferservice.util;

import com.bootcamp.transferservice.model.Client;
import com.bootcamp.transferservice.model.Credit;
import com.bootcamp.transferservice.model.CreditCard;
import com.bootcamp.transferservice.model.dto.ClientDto;
import com.bootcamp.transferservice.model.dto.CreditCardDto;
import com.bootcamp.transferservice.model.dto.CreditDto;

public interface DtoConverter {
  Credit convert(CreditDto creditDto);

  CreditDto convert(Credit credit);

  CreditCard convert(CreditCardDto creditCardDto);

  CreditCardDto convert(CreditCard creditCard);

  Client convert(ClientDto clientDto);

  ClientDto convert(Client client);

}
