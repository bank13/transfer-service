package com.bootcamp.transferservice.util.impl;

import com.bootcamp.transferservice.model.Client;
import com.bootcamp.transferservice.model.Credit;
import com.bootcamp.transferservice.model.CreditCard;
import com.bootcamp.transferservice.model.dto.ClientDto;
import com.bootcamp.transferservice.model.dto.CreditCardDto;
import com.bootcamp.transferservice.model.dto.CreditDto;
import com.bootcamp.transferservice.util.DtoConverter;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MapStructConverter extends DtoConverter {

  MapStructConverter MAPPER = Mappers.getMapper(MapStructConverter.class);

  @Override
  Credit convert(CreditDto creditDto);

  @Override
  CreditDto convert(Credit credit);

  @Override
  CreditCard convert(CreditCardDto creditCardDto);

  @Override
  CreditCardDto convert(CreditCard creditCard);

  @Override
  Client convert(ClientDto clientDto);

  @Override
  ClientDto convert(Client client);
}

