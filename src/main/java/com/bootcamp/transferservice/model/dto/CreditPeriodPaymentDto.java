package com.bootcamp.transferservice.model.dto;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreditPeriodPaymentDto {
  private String creditPeriodPaymentID;
  private String creditID;
  private double finalAmount;
  private double penalties;
  private LocalDateTime paymentStartDate;
  private LocalDateTime paymentEndDate;
  private String status;
  private boolean isDue;
}
