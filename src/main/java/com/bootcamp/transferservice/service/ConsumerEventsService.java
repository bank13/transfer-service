package com.bootcamp.transferservice.service;

import com.bootcamp.kafka.models.events.Event;
import com.bootcamp.kafka.models.events.StringEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class ConsumerEventsService {

  private static final Logger logger = LoggerFactory.getLogger(ConsumerEventsService.class);

  /**
   * consumer method.
   *
   * @param event Event |?|
   */
  @KafkaListener(topics = "incomes",
          containerFactory = "kafkaListenerContainerFactory",
          groupId = "eventMessage")
  public void consumer(Event<?> event) {

    if (event.getClass().isAssignableFrom(StringEvent.class)) {
      StringEvent customEvent = (StringEvent) event;

      String message = String.format("Received created event .... with Id=%s, data=%s",
              customEvent.getId(),
              customEvent.getData());

      System.out.println(message);
      logger.info(message);
    }
  }
}
