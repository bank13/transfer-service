package com.bootcamp.transferservice.notifications;

import com.bootcamp.kafka.models.events.Event;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

  private final String bootstrapAddress = "127.0.0.1:9092";

  /**
   * consumerFactory method.
   *
   * @return
   */
  @Bean
  public ConsumerFactory<String, Event<?>> consumerFactory() {

    Map<String, String> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "eventMessage");
    props.put(JsonDeserializer.TRUSTED_PACKAGES, "*");

    JsonDeserializer<Event<?>> jsonDeserializer = new JsonDeserializer<>();

    return new DefaultKafkaConsumerFactory(
            props,
            new StringDeserializer(),
            jsonDeserializer);
  }

  /**
   * ConcurrentKafkaListenerContainerFactory method.
   *
   * @return
   */
  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, Event<?>> kafkaListenerContainerFactory() {

    ConcurrentKafkaListenerContainerFactory<String, Event<?>> factory =
            new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    return factory;
  }
}
